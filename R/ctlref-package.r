#' ctlref: instructions for creating and using tidy reference annotations 
#'
#' The ctlref package facilitates creating a simpler, tidier version of essential
#' genome annotation references straight from reliable sources, primarily Ensembl
#' and Bioconductor. This package distills annotations to the bare essentials
#' for maximal portability, accessibility, and translation of results. Below
#' are instructions for acquiring necessary files to parse and create your own
#' reference annotations for use in R and other applications.
#'
#' The ctlref package builds simpler, tidy tibbles of reference annotations that
#' are useful for analysis of biological data. However, certain files are required
#' (recommended) for use with this package.
#'
#' The primary reference source used for this 
#' package is based on the [Ensembl](http://uswest.ensembl.org/index.html) reference.
#' The required annotations to be downloaded from Ensembl are as follows:
#'
#' * _sequence report_ : contains essential information on chromosome names and
#'   lengths; it contains full contigs as well as additional scaffolds that may or
#'   may not be included in downstream reference reconstruction.
#' * _GTF file_ : maps coordinates to transcripts and genes in the gene transfer format
#'
#' Additionally, from Bioconductor:
#'
#' * transcript database object `org.{organism-abbreviation}.eg.db` : where the
#' organism abbreviation is two letters ('Hs' for Homo sapiens, 'Mm' for Mus musculus for example).
#' This object is required for mapping from Ensembl identifiers to symbols and ENTREZ identifiers
#' and available from Bioconductor.
#'
#' Finally, it is recommended that all references be from the same source. In particular for
#' sequencing applications, it is recommended to also use the cDNA/DNA FASTA files
#' available from the [Ensembl FTP repository](http://uswest.ensembl.org/info/data/ftp/index.html/)
#' to build reference indexes for alignment applications.
#'
#'
#'
#' 
#' The GTF file can be acquired directly from the
#' [Ensembl FTP repository](http://uswest.ensembl.org/info/data/ftp/index.html/). Simply
#' navigate to the link on the page for your desired organism, for example for the
#' [Homo sapiens GTF](ftp://ftp.ensembl.org/pub/release-91/gtf/homo_sapiens),
#' and download the '{organism}.{build}.{patch}.gtf.gz' file.
#'
#'
#'
#' 
#' From the [Ensembl FTP repository](http://uswest.ensembl.org/info/data/ftp/index.html/),
#' click on organism name's link, as in for [Mouse](http://uswest.ensembl.org/Mus_musculus/Info/Index),
#' then see under the "Gene annotation" section a link for
#' [more about this genebuild](http://uswest.ensembl.org/Mus_musculus/Info/Annotation). Finally,
#' go to the [assembly information link](https://www.ebi.ac.uk/ena/data/view/GCA_000001635.7), where
#' you can then go to "View" for the [sequence report](ftp://ftp.ebi.ac.uk/pub/databases/ena/assembly/GCA_000/GCA_000001/GCA_000001635.7_sequence_report.txt) and download it.
#'
#'
#' 
#' 
#' The transcript database can be installed via Bioconductor by invoking
#' `source('https://bioconductor.org/biocLite.R') followed by `biocLite("your-organism-db")`.
#' to install your desired organism transcript database. For example, for human and mouse,
#' the databases are `org.Hs.eg.db` and `org.Mm.eg.db`, respectively. To use the
#' database, invoke `library(your-organism-db)`, and then the database is accessible as
#' `your-organism-db`, which can be reassigned to a new object if desired.
#'
#' 
#'
#' Note that the package uses the default chromosome names, which from Ensembl, are
#' simply numeric without a leading 'chr'. To append a leading 'chr', simply do so
#' by running `mutate(ref, chrom = paste0('chr', chrom))`. However, take caution
#' when doing this and use the same convention across your alignment and
#' analysis references. Many programs do require the leading 'chr', however, I leave this
#' up to the user as to whether to do so or not.
#'
#'
#' 
#' @name ctlref
#' @docType package
NULL
