## Annotations are derived from the following source files from Ensembl and Assembly:
##
## [1] "Homo_sapiens.GCA_000001405.25_sequence_report.txt"
## [2] "Homo_sapiens.GRCh38.91.chr.gtf"                   
## [3] "Mus_musculus.GCA_000001635.7_sequence_report.txt" 
## [4] "Mus_musculus.GRCm38.91.chr.gtf.gz"
##
## Note that leading 'chr' was appended to the included datasets.
